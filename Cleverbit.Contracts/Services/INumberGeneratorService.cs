﻿using System;

namespace CleverBit.Contracts.Services
{
    public interface INumberGeneratorService 
    {
        short GenerateNumber(short min = 1, short max = 100);
    }
}
