﻿using CleverBit.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBit.Contracts.Services
{
    public interface IMatchOddService
    {
        MatchOddsDto GetMatchOdd(int MatchId);
    }
}
