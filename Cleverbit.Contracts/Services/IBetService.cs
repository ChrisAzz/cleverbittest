﻿using CleverBit.Contracts.Models;
using CleverBit.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBit.Contracts.Services
{
    public interface IBetService
    {
        bool AlreadyBet(string userId, int MatchId);
        int PerformBet(string userId, BetRequest betRequest);
    }
}
