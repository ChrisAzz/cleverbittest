﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBit.Contracts.Models
{
    public class BetRequest
    {
        public int MatchId { get; set; }
        public decimal Amount { get; set; }
        public string Decision { get; set; }
    }
}
