﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleverBit.Hubs
{
    public class MatchOddsHub : Hub
    {
        public void Send(string matchId, string odd1, string oddX, string odd2)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<MatchOddsHub>();
            context.Clients.All.broadcastMessage(matchId, odd1, oddX, odd2);
        }
    }
}
