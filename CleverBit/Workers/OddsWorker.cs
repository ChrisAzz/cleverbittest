﻿using Autofac;
using CleverBit.Contracts.Services;
using CleverBit.DependencyInjection;
using CleverBit.Hubs;
using CleverBit.Services;
using System;
using System.Configuration;
using System.Threading;

namespace CleverBit.Workers
{
    public class OddsWorker
    {
        private readonly System.ComponentModel.BackgroundWorker _oddsWorkerProcess;

        public OddsWorker()
        {
            _oddsWorkerProcess = new System.ComponentModel.BackgroundWorker();
        }

        public void StartWorker()
        {
            _oddsWorkerProcess.WorkerSupportsCancellation = true;
            _oddsWorkerProcess.DoWork += OddsWorkerProcess_DoWork;
            _oddsWorkerProcess.RunWorkerAsync();
        }

        private static void OddsWorkerProcess_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var randomizer = AutofacResolver.GetContainer().Resolve<INumberGeneratorService>();

            for (; ; )
            {
                if (e.Cancel)
                {
                    return;
                }

                var probability1 = randomizer.GenerateNumber();
                var probabilityX = randomizer.GenerateNumber(max: (short)(100 - probability1 - 1));
                var probability2 = 100 - probability1 - probabilityX;

                var odd1 = ((decimal)1 / probability1) * 100;
                var oddX = ((decimal)1 / probabilityX) * 100;
                var odd2 = ((decimal)1 / probability2) * 100;

                //Store latest odds to DB
                var matchOddDto = new Data.Contracts.Models.MatchOddsDto { MatchID = 1, AwayWins = string.Format("{0:.##}", odd2), Draw = string.Format("{0:.##}", oddX), HomeWins = string.Format("{0:.##}", odd1) };
                Data.MatchOdds.SaveLatestOdds(matchOddDto);

                //Publish to front via SignalR
                new MatchOddsHub().Send(matchOddDto.MatchID.ToString(), matchOddDto.HomeWins, matchOddDto.Draw, matchOddDto.AwayWins);
        
                Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings.Get("OddsRefreshRateMS")));
            }
        }

        public void StopWorker()
        {
            _oddsWorkerProcess.CancelAsync();
        }
    }
}
