﻿using Autofac;
using CleverBit.Contracts.Services;
using CleverBit.Services;

namespace CleverBit.DependencyInjection
{
    public class AutofacResolver
    {
        private static IContainer _container;
        public static IContainer GetContainer()
        {
            if (_container == null)
            {
                var builder = new ContainerBuilder();
                builder.RegisterType<MatchOddService>().As<IMatchOddService>();
                builder.RegisterType<NumberGeneratorService>().As<INumberGeneratorService>();
                builder.RegisterType<BetService>().As<IBetService>();
                _container = builder.Build();
            }
            return _container;
        }
    }
}
