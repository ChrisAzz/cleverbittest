﻿using CleverBit.Contracts;
using CleverBit.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CleverBit.Services
{
    public class NumberGeneratorService : INumberGeneratorService
    {
        public short GenerateNumber(short min = 1, short max = 100)
        {
            Random random = new Random();
            return (short)random.Next(min, max);
        }
    }
}
