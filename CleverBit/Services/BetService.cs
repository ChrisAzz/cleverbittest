﻿using Autofac;
using CleverBit.Contracts.Models;
using CleverBit.Contracts.Services;
using CleverBit.Data;
using CleverBit.Data.Contracts.Models;
using CleverBit.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleverBit.Services
{
    public class BetService : IBetService
    {
        public bool AlreadyBet(string userId, int MatchId)
        {
            return Bets.GetBet(userId, MatchId) != null;
        }

        public int PerformBet(string userId, BetRequest betRequest)
        {
            var matchOddService = AutofacResolver.GetContainer().Resolve<IMatchOddService>();
            var matchOdd = matchOddService.GetMatchOdd(betRequest.MatchId);

            var newBet = new BetDto()
            {
                UserId = userId,
                MatchId = betRequest.MatchId,
                Decision = betRequest.Decision,
                Amount = betRequest.Amount,
                AmountToWin = CalculateWinnableAmount(matchOdd, betRequest)
            };

            return Bets.SaveBet(newBet);
        }

        private decimal CalculateWinnableAmount(MatchOddsDto odds, BetRequest bet)
        {
            switch (bet.Decision)
            {
                case "1": return Decimal.Parse(odds.HomeWins) * bet.Amount;
                case "X": return Decimal.Parse(odds.Draw) * bet.Amount;
                case "2": return Decimal.Parse(odds.AwayWins) * bet.Amount;
                default:return 0;
            }
        }
    }
}
