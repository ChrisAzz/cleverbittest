﻿using CleverBit.Contracts.Services;
using CleverBit.Data;
using CleverBit.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleverBit.Services
{
    public class MatchOddService : IMatchOddService
    {
        public MatchOddsDto GetMatchOdd(int MatchId)
        {
            return MatchOdds.GetLatestOdds(MatchId);
        }
    }
}
