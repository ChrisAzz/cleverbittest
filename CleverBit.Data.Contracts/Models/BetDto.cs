﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBit.Data.Contracts.Models
{
    public class BetDto
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int MatchId { get; set; }
        public decimal Amount { get; set; }
        public string Decision { get; set; }
        public decimal AmountToWin { get; set; }
    }
}
