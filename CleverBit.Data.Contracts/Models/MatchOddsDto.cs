﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBit.Data.Contracts.Models
{
    public class MatchOddsDto
    {
        public int MatchID { get; set; }
        public string HomeWins { get; set; }
        public string Draw { get; set; }
        public string AwayWins { get; set; }

    }
}
