﻿using CleverBit.Contracts.Services;
using CleverBit.Data;
using CleverBit.Data.Contracts.Models;
using CleverBit.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CleverBit.DependencyInjection;
using Autofac;

namespace CleverBit.Host.Controllers
{
    
    public class MatchOddController : ApiController
    { 
        // GET: api/MatchOdd/5
        [HttpGet]
        public MatchOddsDto Get(int id)
        {
            var matchOddService = AutofacResolver.GetContainer().Resolve<IMatchOddService>();
            return matchOddService.GetMatchOdd(id);
        }
    }
}
