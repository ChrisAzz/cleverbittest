﻿using CleverBit.Contracts.Models;
using CleverBit.Contracts.Services;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using CleverBit.DependencyInjection;
using Autofac;

namespace CleverBit.Host.Controllers
{
    public class BetsController : ApiController
    {
        [Authorize]
        [HttpPost]
        [ActionName("PlaceBet")]
        public int PlaceBet([FromBody]BetRequest betRequest)
        {
            var betService = AutofacResolver.GetContainer().Resolve<IBetService>();
            if (!betService.AlreadyBet(User.Identity.GetUserId(), betRequest.MatchId))
            {
                return betService.PerformBet(User.Identity.GetUserId(), betRequest);
            }
            return 0;
        }
    }
}
