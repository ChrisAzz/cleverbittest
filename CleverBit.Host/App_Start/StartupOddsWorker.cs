﻿using CleverBit.Workers;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CleverBit.Host
{
    public class StartupOddsWorker
    {
        private static OddsWorker _oddsWorker;
        public static void StartOddsWorker()
        {
            _oddsWorker = new OddsWorker();
            _oddsWorker.StartWorker();
        }

        public static void StopOddsWorker()
        {
            _oddsWorker.StopWorker();
        }
    }
}