﻿using CleverBit.Hubs;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CleverBit.Host
{
    public partial class Startup
    {
        public void ConfigureSignalR(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}