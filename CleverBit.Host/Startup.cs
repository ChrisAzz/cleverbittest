﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CleverBit.Host.Startup))]
[assembly: OwinStartup(typeof(CleverBit.Host.Startup))]
namespace CleverBit.Host
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureSignalR(app);
        }
    }
}
