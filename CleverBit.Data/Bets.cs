﻿using CleverBit.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBit.Data
{
    public class Bets
    {
        public static BetDto GetBet(string userId, int MatchId)
        {
            using (var db = new CleverBitDB())
            {
                var result = db.Bets.SingleOrDefault(b => b.UserId == userId && b.MatchId == MatchId);
                if (result != null)
                {
                    return new BetDto()
                    {
                        Amount = result.Amount,
                        UserId = result.UserId,
                        MatchId = result.MatchId, 
                        AmountToWin = result.AmountToWin,
                        Decision = result.Decision,
                        Id = result.Id
                    };
                }
                return null;
            }
        }

        public static int SaveBet(BetDto bet)
        {
            using (var db = new CleverBitDB())
            {
                var newBet = new Bet()
                {
                    Amount = bet.Amount,
                    UserId = bet.UserId,
                    MatchId = bet.MatchId,
                    AmountToWin = bet.AmountToWin,
                    Decision = bet.Decision
                };
                db.Bets.Add(newBet);
                db.SaveChanges();
                return newBet.Id;
            }
        }
    }
}
