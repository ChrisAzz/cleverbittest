﻿using CleverBit.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace CleverBit.Data
{
    public class MatchOdds
    {
        public static MatchOddsDto GetLatestOdds(int MatchId)
        {
            using (var db = new CleverBitDB())
            {
                var result = db.MatchOdds.SingleOrDefault(b => b.Id == MatchId);
                if (result != null)
                {
                    return new MatchOddsDto()
                    {
                        MatchID = result.Id,
                        AwayWins = result.AwayWins,
                        Draw = result.Draw,
                        HomeWins = result.HomeWins
                    };
                }

                return null;
            }
        }

        public static void SaveLatestOdds(MatchOddsDto MatchOdds)
        {
            using (var db = new CleverBitDB())
            {
                var result = db.MatchOdds.SingleOrDefault(b => b.Id == MatchOdds.MatchID);
                if (result != null)
                {
                    result.AwayWins = MatchOdds.AwayWins;
                    result.HomeWins = MatchOdds.HomeWins;
                    result.Draw = MatchOdds.Draw;
                    db.SaveChanges();
                }
            }
        }
    }
}
